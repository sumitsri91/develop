<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'woocommerce' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'W31come@123' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '??sI}})/t(w7n&S9KJY1-B]a1U<)rQ4vi8!-~]U;~jPSy1X$%azo[mWM()fzJz5d' );
define( 'SECURE_AUTH_KEY',  '>L5>x .w^~K]/FQ{^sJwH7 @,F%=8,M|a]FnHfyUEc/ZJQ?Vl~t*<Dty5;nsY8nA' );
define( 'LOGGED_IN_KEY',    'w!Ufvbuuigwk_c7nERc jf/[v)e%2,4am(=hbLnh(o$qo;>aDV1zHApk@A+}E!Zg' );
define( 'NONCE_KEY',        '{lAdb,NlUIHiB8HpzB!^}(?)ov(FPad=j`k:m1T@1qtMi_u|~OyT,CLkd|?&qE!L' );
define( 'AUTH_SALT',        'aU(4y*jdG&6h#Kn>fwIXGA~}4y=+{Y>*Lf(KP xfUQ`d~?71 >w*VYjKUs0`im{n' );
define( 'SECURE_AUTH_SALT', 'vU.~%}Z,39ue2J{Y8lkQA|S04Xjx&Q<Md/+q^-k?pz^%R^{@a<~:H|y7cxt;_;W:' );
define( 'LOGGED_IN_SALT',   'eA`ci-B(bTtj]ZmQtfUS>NpFA3~+DUXG:kiziV^^aqR$d9zM=n q2K=q-%bm-hrw' );
define( 'NONCE_SALT',       'Y]g4Qr_d4c [CTsy~/AM3EM*8dvR_=]`birGyvcC9#B`Dae ;lo7KF3BIf|`2A.,' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'all_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}
	define(UPLOADS, 'wp-content/uploads');
/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
